import React, { Component } from 'react';


class France extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataUser: []
        }
    }
    componentDidMount() {
        fetch('https://randomuser.me/api/?nat=FR&results=10')
            .then(res => res.json())
            .then(listJson => listJson.results.map(data => ({
                id: `${data.id.name}`,
                firstName: `${data.name.first}`,
                lastName: `${data.name.last}`,
                location: `${data.location.state}, ${data.nat}`,
                thumbnail: `${data.picture.large}`,
            }
            )))
            .then(dataUser => this.setState({
                dataUser
            }))
            .then(error => console.log('parsing fail', error))
    }

    render() {
        const { dataUser } = this.state
        return (
            <div>
                <div className="boxWhite">
                    <h2 className="title">France User</h2>
                    {
                        dataUser.length >= 0 ? dataUser.map(item => {
                            const { id, firstName, lastName, location, thumbnail } = item
                            return (
                                <div key={id} className="bgCircle">
                                    <center><img src={thumbnail} alt={firstName} className="circle" /></center><br />
                                    <div className="info">
                                        {firstName} {lastName} <br />
                                        {location}
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
            </div>
        )
    }

}


export default France;