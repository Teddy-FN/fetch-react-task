// https://randomuser.me/api/?nat=DE&results=10

import React, { Component } from 'react';



class Germany extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userList: []
        }
    }

    componentDidMount() {
        fetch('https://randomuser.me/api/?nat=DE&results=10')
            .then(result => result.json())
            .then(listDataUser => listDataUser.results.map(dataUser => ({
                id: `${dataUser.id.name}`,
                firstName: `${dataUser.name.first}`,
                lastName: `${dataUser.name.last}`,
                location: `${dataUser.location.state}, ${dataUser.nat}`,
                thumbnail: `${dataUser.picture.large}`
            }
            )))
            .then(userList => this.setState({
                userList
            }))
            .then(error => console.log('parsing fail', error))
    }

    render() {
        const { userList } = this.state
        return (
            <div>
                <div className="boxWhite">
                    <h2 className="title">Germany User</h2>
                    {
                        userList.length >= 0 ? userList.map(item => {
                            const { id, firstName, lastName, location, thumbnail } = item
                            return (
                                <div key={id} className="bgCircle">
                                    <center><img src={thumbnail} alt={firstName} className="circle" /></center><br />
                                    <div className="info">
                                        {firstName} {lastName} <br />
                                        {location}
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
            </div>
        )
    }
}



export default Germany;