import React, { Component } from 'react';



class Turkey extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataList: []
        }
    }

    componentDidMount() {
        fetch('https://randomuser.me/api/?nat=TR,&results=10')
            .then(result => result.json())
            .then(listData => listData.results.map(item => ({
                id: `${item.id.name}`,
                firtsName: `${item.name.firts}`,
                lastName: `${item.name.last}`,
                location: `${item.location.state}, ${item.nat}`,
                thumbnail: `${item.picture.large}`
            }
            )))
            .then(dataList => this.setState({
                // ARAHKAN KE ARRAY
                dataList
            }))
            .then(error => console.log('Pairing Fail', error));
    }


    render() {
        const { dataList } = this.state
        return (
            <div>
                <div className="boxWhite">
                    <h2 className="title">Turkey User</h2>
                    {
                        dataList.length > 0 ? dataList.map(item => {
                            const { id, firstName, lastName, location, thumbnail } = item
                            return (
                                <div key={id} className="bgCircle">
                                    <center><img src={thumbnail} alt={firstName} className="circle" /></center><br />
                                    <div className="info">
                                        {firstName} {lastName} <br />
                                        {location}
                                    </div>
                                </div>
                            )
                        }) : null
                    }
                </div>
            </div>
        )
    }

}

export default Turkey;