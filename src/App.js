// import logo from './logo.svg';
import React, { useState } from 'react';
import './App.css';
import './style.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Switch, Route, Link } from 'react-router-dom';
import { Nav, NavItem } from 'reactstrap';
import Random from './pages/Random/Random'
import France from '../src/France/France';
import Turkey from './pages/Turkey/Turkey';
import Germany from './pages/Germany/Germany';


// Reactstrap 
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  NavbarText
} from 'reactstrap';



function App(props) {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link to='/random' className="list">Random</Link>
            </NavItem>
            <NavItem>
              <Link to='/france' className="list">France</Link>
            </NavItem>
            <NavItem>
              <Link to='/turkey' className="list">Turkey</Link>
            </NavItem>
            <NavItem>
              <Link to='/germany' className="list">Germany</Link>
            </NavItem>
          </Nav>
          <NavbarText>Simple Text</NavbarText>
        </Collapse>
      </Navbar>

      <Switch>
        <Route path='/random'>
          <Random />
        </Route>
        <Route path='/france'>
          <France />
        </Route>
        <Route path='/turkey'>
          <Turkey />
        </Route>
        <Route path="/germany">
          <Germany />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
